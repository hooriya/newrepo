<?PHP
class Flower{
public $color;
public $name;
public function __construct($color,$name)
{
    $this->color = $color;
    $this->name = $name;
}
    public function message()
    {
        return "I have a ". $this->color ." ". $this->name . "!";
    }
}
$myFlower = new Flower("Red","Rose");
echo $myFlower -> message();
echo "<br>";
$myFlower = new Flower("Yellow","Sunflower");
echo $myFlower -> message();
?>
